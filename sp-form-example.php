<?php
/*
Plugin Name: Example Contact Form Plugin
Description: Simple non-bloated WordPress Contact Form
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
global $wpdb;

function html_form_code() {
  echo '<form action="' . plugins_url() . '/sp-form-example/insert.php" method="post">';
  echo '<p>';
  echo 'Your Name (required) <br/>';
  echo '<input type="text" name="cf-name" pattern="[a-zA-Z0-9 ]+" value="' . ( isset( $_POST["cf-name"] ) ? esc_attr( $_POST["cf-name"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Your Email (required) <br/>';
  echo '<input type="email" name="cf-email" value="' . ( isset( $_POST["cf-email"] ) ? esc_attr( $_POST["cf-email"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Phone Number (required) <br/>';
  echo '<input type="text" name="cf-phonenumber" pattern="[0-9 ]+" value="' . ( isset( $_POST["cf-phonenumber"] ) ? esc_attr( $_POST["cf-phonenumber"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Your Message (required) <br/>';
  echo '<textarea rows="10" cols="35" name="cf-testimonial">' . ( isset( $_POST["cf-testimonial"] ) ? esc_attr( $_POST["cf-testimonial"] ) : '' ) . '</textarea>';
  echo '</p>';
  echo '<p><input type="submit" name="cf-submitted" value="Send"></p>';
  echo '</form>';
}

function cf_shortcode() {
  ob_start();
  html_form_code();
  return ob_get_clean();
}

add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );

?>